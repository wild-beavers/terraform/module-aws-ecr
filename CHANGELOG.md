## 0.2.1

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks

## 0.2.0

- feat: outputs KMS and replica KMS `alias` and `alias_arn`
- feat: adds `var.iam_policy_external_repositories_arns` to inject external repositories to generate the policies
- feat: adds `var.iam_policy_external_repositories_enabled` to toggle `var.iam_policy_external_repositories_arns`
- feat: allows to restrict resources accesses through account IDs via `var.iam_policy_restrict_by_account_ids`
- feat: allows to restrict replication for repositories with a specific prefix using `var.replica_filter_prefix`
- feat: allows account IDs to be `*` in IAM entities variables
- feat: allows `ecr:DescribeImageScanFindings` among read policies
- fix: renames `ecr_iam_policy_export_json` to `repositories_iam_policy_export_json`
- fix: repository name cannot start with a forward slash

## 0.1.1

- fix: makes `iam_policy_jsons` output keys predictable

## 0.1.0

- feat: adds initial module with registry, repositories, kms and iam
- test: adds `registry` example
- test: adds `default` with various examples

## 0.0.0

- tech: initial template code
