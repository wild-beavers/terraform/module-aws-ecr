# terraform-module-ecr

Terraform module to deploy ECR and related resources.

## Limitations

This module only handles a single replica, through an additional provider named `aws.replica`.
This is due to limitations in the KMS service.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3 |
| aws | >= 5.9 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5.9 |
| aws.replica | >= 5.9 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ecr_lifecycle_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_lifecycle_policy) | resource |
| [aws_ecr_pull_through_cache_rule.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_pull_through_cache_rule) | resource |
| [aws_ecr_registry_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_registry_policy) | resource |
| [aws_ecr_registry_scanning_configuration.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_registry_scanning_configuration) | resource |
| [aws_ecr_replication_configuration.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_replication_configuration) | resource |
| [aws_ecr_repository.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository) | resource |
| [aws_ecr_repository.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository) | resource |
| [aws_ecr_repository_policy.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository_policy) | resource |
| [aws_ecr_repository_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository_policy) | resource |
| [aws_iam_group_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_kms_alias.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key_policy.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_key_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_replica_key.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_replica_key) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_group.policy_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_group) | data source |
| [aws_iam_policy_document.ecr_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_external_repositories](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_full](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_full_statement](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_internal_repositories](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_internal_repositories_base](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read_statement](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read_write_delete](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read_write_delete_statement](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecr_read_write_statement](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.external_ecr_kms_rw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.external_ecr_kms_rwd](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.internal_ecr_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ecr\_registry\_policy\_json | Policy JSON for the whole ECR registry. | `string` | `null` | no |
| ecr\_registry\_pull\_through\_cache\_rule | Allows registry to pull public images in its private registry.<br/><br/>  * ecr\_repository\_prefix   (required, string):                   The repository name prefix to use when caching images from the source registry.<br/>  * upstream\_registry\_url   (optional, string, "public.ecr.aws"): The registry URL of the upstream public registry to use as the source. | <pre>object({<br/>    ecr_repository_prefix = string<br/><br/>    upstream_registry_url = optional(string, "public.ecr.aws")<br/>  })</pre> | `null` | no |
| ecr\_registry\_scanning\_configuration\_rules | Setup scanning rules for all repositories in the registry.<br/>Keys are scan\_frequency. Can be `SCAN_ON_PUSH`, `CONTINUOUS_SCAN`, or `MANUAL`.<br/><br/>  * repository\_filters (required, set(string)): String to filter repositories, see pattern regex [here](https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_ScanningRepositoryFilter.html). | <pre>map(object({<br/>    repository_filters = set(string)<br/>  }))</pre> | `null` | no |
| force\_delete | If `true`, will delete the resources of this module even if they contain data. E.g. if `true`, ECR will be deleted even if they contain images. Defaults to `false`. | `bool` | `false` | no |
| iam\_admin\_arns | ARNs of the Administrators for all the resources of this module. If you set any policy, this is mandatory. Secrets will grant unrestricted access to these ARNs. Wildcards are authorized. `var.iam_policy_attach_to_principals` will have no effect on these resources. Keys are free values. | `map(string)` | `null` | no |
| iam\_policy\_attach\_to\_principals | Whether to attach auto-generated policies to given `var.iam_policy_group_arns`, `var.iam_policy_user_arns` and `var.iam_policy_role_arns`. For more complex use cases, this be toggled of in favor of `…iam_policy_export_json` variables. | `bool` | `false` | no |
| iam\_policy\_description | Description of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `string` | `null` | no |
| iam\_policy\_external\_repositories\_arns | ARNs of ECR repositories created outside this module to compose the external policies with. Along with repositories created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values. | `map(string)` | `null` | no |
| iam\_policy\_external\_repositories\_enabled | When `true` user of this module is expected to set `var.iam_policy_external_repositories_arn` with non null values. | `bool` | `false` | no |
| iam\_policy\_group\_arns | Restrict the access to resources of this module containing policies to the given groups. Used in addition of `var.iam_policy_user_arns` and `var.iam_policy_role_arns`. Wildcards are **not** accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs. Keys are free values. | `map(string)` | `null` | no |
| iam\_policy\_name | Name of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `string` | `null` | no |
| iam\_policy\_path | Path of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `string` | `null` | no |
| iam\_policy\_restrict\_by\_account\_ids | Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `null` | no |
| iam\_policy\_role\_arns | Restrict the access to resources of this module containing policies to the given roles. Used in addition of `var.iam_policy_group_arns` and `var.iam_policy_user_arns`. Wildcards and variables are accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs, but will not work with wildcards. Keys are free values. | `map(string)` | `null` | no |
| iam\_policy\_sid\_prefix | Use a prefix for all `Sid` of all the policies - internal and external - created by this module. | `string` | `null` | no |
| iam\_policy\_tags | Tags of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `map(string)` | `null` | no |
| iam\_policy\_user\_arns | Restrict the access to resources of this module containing policies to the given users. Used in addition of `var.iam_policy_group_arns` and `var.iam_policy_role_arns`. Wildcards and variables are accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs, but will not work with wildcards. Keys are free values. | `map(string)` | `null` | no |
| kms\_key | Manage the KMS key for the repositories.<br/>If given and and `var.replica_enabled`, this key will be use also for the replica, unless `var.kms_replica_key_id` is set.<br/>Keys are free values.<br/><br/>  * alias                   (required, string):             KMS key alias; Display name of the KMS key. Omit the `alias/`.<br/>  * policy\_json             (optional, string):             Internal policy in JSON to attach to the KMS key. This module will grant the bare minimum for ECR and merge it with this policy.<br/>  * default\_policy          (optional, bool, false):        Whether to use restricted policy for the key or the AWS default open policy.<br/>  * description             (optional, string, "For ECR."): Description of the key.<br/>  * rotation\_enabled        (optional, bool, true):         Whether to automatically rotate the KMS key linked to the ECR.<br/>  * deletion\_window\_in\_days (optional, number, 7):          The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br/>  * tags                    (optional, map(string)):        Tags to be used by the KMS key of this module. | <pre>object({<br/>    alias                   = string<br/>    policy_json             = optional(string)<br/>    default_policy          = optional(bool, false)<br/>    description             = optional(string, "For ECR.")<br/>    rotation_enabled        = optional(bool, true)<br/>    deletion_window_in_days = optional(number, 7)<br/>    tags                    = optional(map(string), {})<br/>  })</pre> | `null` | no |
| kms\_key\_external\_enabled | If `true`, you declare `kms_key_id` and `kms_replica_key_id` are not empty. https://github.com/hashicorp/terraform/issues/33620. | `bool` | `false` | no |
| kms\_key\_id | ARN, Key ID, or Alias of the AWS KMS to be used to encrypt the resources data of this module. If you don't specify this value or `var.kms_key`, then ECR defaults to using the AWS account's default CMK (the one named `aws/ecr`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time. | `string` | `null` | no |
| kms\_replica\_key\_id | ARN, Key ID, or Alias of the AWS KMS key within the region ECR is replicated to. If one is not specified, then ECR defaults to using the AWS account's default KMS key. If `null` and `kms_key_alias` is set, a replica of the KMS key will be used. Ignored anyway if `var.replica_enabled` is `false`. | `string` | `null` | no |
| kms\_tags | Tags to be shared with all KMS resources. Will be merge with `var.tags`. | `map(string)` | `null` | no |
| replica\_enabled | If `true`, ECR will be replicated into the `aws.replica` region. | `bool` | `false` | no |
| replica\_filter\_prefix | Prefix of the ECR to replicate to the the given `aws.replica` region if `var.replica_enabled` is `true`. If not given, all private repositories of the account will be replicated. | `string` | `null` | no |
| repositories | For ECR repositories.<br/>Keys are free values.<br/><br/>  * name                   (required, string):            Name of the repository.<br/><br/>  * image\_tag\_mutability   (optional, string, "MUTABLE"): The tag mutability setting for the repository. Must be one of: `MUTABLE` or `IMMUTABLE`.<br/>  * lifecycle\_policy       (optional, string):            The policy document. This is a JSON formatted string. See more details about [Policy Parameters](http://docs.aws.amazon.com/AmazonECR/latest/userguide/LifecyclePolicies.html#lifecycle_policy_parameters) in the official AWS docs.<br/>  * policy                 (optional, string):            The policy document. This is a JSON formatted string. Will be merged with any policy statement generated through `policy_scope`.<br/>  * policy\_scope           (optional, string, "NONE"):    If provided, will attach a policy to restrict accesses to the ECR. Will be **merged** with `policy`. Can be `NONE`, `RO`, `RW`, `RWD` or `FULL`. `NONE`, the default, disables this setting. This settings is used for both internal and potential external policies.<br/>  * tags                   (optional, map(string)):       A map of tags to assign to the ECR. | <pre>map(object({<br/>    name = string<br/><br/>    image_tag_mutability = optional(string, "MUTABLE")<br/>    lifecycle_policy     = optional(string)<br/>    policy               = optional(string)<br/>    policy_scope         = optional(string, "NONE")<br/>    tags                 = optional(map(string))<br/>  }))</pre> | `null` | no |
| repositories\_iam\_policy\_export\_json | Whether to export data IAM policies as JSON strings. Each `var.ecr_repositories` will export a custom policy to grant data access to them, in accordance to given `policy_scope` and IAM: `var.iam_policy_group_arns`, `var.iam_policy_user_arns` and `var.iam_policy_role_arns`. | `bool` | `false` | no |
| repositories\_tags | Tags to be shared with all ECR resources. Will be merge with `var.tags`. | `map(string)` | `null` | no |
| tags | Tags to be shared with all resources of this module. | `map(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| ecr\_repositories | n/a |
| iam\_policy | n/a |
| iam\_policy\_jsons | n/a |
| kms\_key | n/a |
<!-- END_TF_DOCS -->

## Versioning

This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks

This repository uses [pre-commit](https://pre-commit.com/) hooks.
