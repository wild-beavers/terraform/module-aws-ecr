#####
# Variables
#####

variable "ecr_registry_policy_json" {
  type        = string
  description = "Policy JSON for the whole ECR registry."
  default     = null

  validation {
    condition     = var.ecr_registry_policy_json == null || try(jsondecode(var.ecr_registry_policy_json), null) != null
    error_message = "“var.ecr_registry_policy_json” is invalid."
  }
}

variable "ecr_registry_scanning_configuration_rules" {
  description = <<-DOCUMENTATION
Setup scanning rules for all repositories in the registry.
Keys are scan_frequency. Can be `SCAN_ON_PUSH`, `CONTINUOUS_SCAN`, or `MANUAL`.

  * repository_filters (required, set(string)): String to filter repositories, see pattern regex [here](https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_ScanningRepositoryFilter.html).
DOCUMENTATION
  type = map(object({
    repository_filters = set(string)
  }))
  default = null

  validation {
    condition = ((!contains([
      for key, rule in coalesce(var.ecr_registry_scanning_configuration_rules, {}) :
      (
        contains(["SCAN_ON_PUSH", "CONTINUOUS_SCAN", "MANUAL"], key) &&
        length(rule.repository_filters) >= 1
      )
    ], false)))
    error_message = "One or more “var.ecr_registry_scanning_configuration_rules” are invalid."
  }
}

variable "ecr_registry_pull_through_cache_rule" {
  description = <<-DOCUMENTATION
Allows registry to pull public images in its private registry.

  * ecr_repository_prefix   (required, string):                   The repository name prefix to use when caching images from the source registry.
  * upstream_registry_url   (optional, string, "public.ecr.aws"): The registry URL of the upstream public registry to use as the source.
DOCUMENTATION
  type = object({
    ecr_repository_prefix = string

    upstream_registry_url = optional(string, "public.ecr.aws")
  })
  default = null

  validation {
    condition     = var.ecr_registry_pull_through_cache_rule == null || can(regex("^[a-z][0-9a-z\\/\\._-]{2,20}$", try(var.ecr_registry_pull_through_cache_rule.ecr_repository_prefix, "valid")))
    error_message = "“var.ecr_registry_pull_through_cache_rule” is invalid."
  }
}

variable "replica_filter_prefix" {
  description = "Prefix of the ECR to replicate to the the given `aws.replica` region if `var.replica_enabled` is `true`. If not given, all private repositories of the account will be replicated."
  type        = string
  default     = null

  validation {
    condition     = var.replica_filter_prefix == null || can(regex("^[a-z][0-9a-z\\/\\._-]{2,20}$", try(var.replica_filter_prefix, "valid")))
    error_message = "“var.replica_filter_prefix” is invalid."
  }
}

#####
# Resources
#####

resource "aws_ecr_replication_configuration" "this" {
  for_each = var.replica_enabled ? { 0 = 0 } : {}

  dynamic "replication_configuration" {
    for_each = [data.aws_region.replica["0"].name]

    content {
      rule {
        destination {
          region      = replication_configuration.value
          registry_id = data.aws_caller_identity.current.account_id
        }

        dynamic "repository_filter" {
          for_each = var.replica_filter_prefix != null ? { 0 = 0 } : {}

          content {
            filter      = var.replica_filter_prefix
            filter_type = "PREFIX_MATCH"
          }
        }
      }
    }
  }
}

resource "aws_ecr_registry_policy" "this" {
  for_each = var.ecr_registry_policy_json != null ? { 0 = 0 } : {}

  policy = var.ecr_registry_policy_json
}

resource "aws_ecr_registry_scanning_configuration" "this" {
  for_each = var.ecr_registry_scanning_configuration_rules != null ? { 0 = 0 } : {}

  scan_type = "ENHANCED"

  dynamic "rule" {
    for_each = var.ecr_registry_scanning_configuration_rules

    content {
      scan_frequency = rule.key

      dynamic "repository_filter" {
        for_each = rule.value.repository_filters

        content {
          filter      = repository_filter.value
          filter_type = "WILDCARD"
        }
      }
    }
  }
}

resource "aws_ecr_pull_through_cache_rule" "this" {
  for_each = var.ecr_registry_pull_through_cache_rule != null ? { 0 = var.ecr_registry_pull_through_cache_rule } : {}

  ecr_repository_prefix = each.value.ecr_repository_prefix
  upstream_registry_url = each.value.upstream_registry_url
}
