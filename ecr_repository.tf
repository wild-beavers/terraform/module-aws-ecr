#####
# Variables
#####

variable "repositories_tags" {
  description = "Tags to be shared with all ECR resources. Will be merge with `var.tags`."
  type        = map(string)
  default     = null
}

variable "repositories" {
  description = <<-DOCUMENTATION
For ECR repositories.
Keys are free values.

  * name                   (required, string):            Name of the repository.

  * image_tag_mutability   (optional, string, "MUTABLE"): The tag mutability setting for the repository. Must be one of: `MUTABLE` or `IMMUTABLE`.
  * lifecycle_policy       (optional, string):            The policy document. This is a JSON formatted string. See more details about [Policy Parameters](http://docs.aws.amazon.com/AmazonECR/latest/userguide/LifecyclePolicies.html#lifecycle_policy_parameters) in the official AWS docs.
  * policy                 (optional, string):            The policy document. This is a JSON formatted string. Will be merged with any policy statement generated through `policy_scope`.
  * policy_scope           (optional, string, "NONE"):    If provided, will attach a policy to restrict accesses to the ECR. Will be **merged** with `policy`. Can be `NONE`, `RO`, `RW`, `RWD` or `FULL`. `NONE`, the default, disables this setting. This settings is used for both internal and potential external policies.
  * tags                   (optional, map(string)):       A map of tags to assign to the ECR.
DOCUMENTATION
  type = map(object({
    name = string

    image_tag_mutability = optional(string, "MUTABLE")
    lifecycle_policy     = optional(string)
    policy               = optional(string)
    policy_scope         = optional(string, "NONE")
    tags                 = optional(map(string))
  }))
  default = null

  validation {
    condition = ((!contains([
      for key, repo in coalesce(var.repositories, {}) :
      (
        can(regex("^[a-z][a-z[0-9a-z\\/\\._-]{1,256}$", repo.name)) &&
        contains(["MUTABLE", "IMMUTABLE"], repo.image_tag_mutability) &&
        (repo.policy_scope == null || try(contains(["NONE", "RO", "RW", "RWD", "FULL"], repo.policy_scope), false)) &&
        (repo.policy == null || try(jsondecode(repo.policy), null) != null) &&
        (repo.lifecycle_policy == null || try(jsondecode(repo.lifecycle_policy), null) != null)
      )
    ], false)))
    error_message = "One or more “var.ecr_repositories” are invalid."
  }
}

variable "repositories_iam_policy_export_json" {
  description = "Whether to export data IAM policies as JSON strings. Each `var.ecr_repositories` will export a custom policy to grant data access to them, in accordance to given `policy_scope` and IAM: `var.iam_policy_group_arns`, `var.iam_policy_user_arns` and `var.iam_policy_role_arns`."
  type        = bool
  default     = false
}

#####
# Locals
#####

locals {
  ecr_repositories_with_policies = {
    for key, repository in coalesce(var.repositories, {}) :
    key => repository if repository.policy != null || repository.policy_scope != "NONE"
  }
  ecr_repositories_with_lifecycle_policies = {
    for key, repository in coalesce(var.repositories, {}) :
    key => repository if repository.lifecycle_policy != null
  }
}

#####
# Resources
#####

resource "aws_ecr_repository" "this" {
  for_each = coalesce(var.repositories, {})

  name                 = each.value.name
  image_tag_mutability = each.value.image_tag_mutability
  force_delete         = var.force_delete
  tags = merge(
    local.tags,
    var.repositories_tags,
    each.value.tags,
  )

  encryption_configuration {
    encryption_type = local.kms_enabled ? "KMS" : "AES256"
    kms_key         = local.kms_enabled ? local.kms_key_id : null
  }
}

resource "aws_ecr_repository_policy" "this" {
  for_each = local.ecr_repositories_with_policies

  repository = aws_ecr_repository.this[each.key].name
  policy     = data.aws_iam_policy_document.ecr_internal_repositories[each.key].json
}

resource "aws_ecr_lifecycle_policy" "this" {
  for_each = local.ecr_repositories_with_lifecycle_policies

  repository = aws_ecr_repository.this[each.key].name
  policy     = each.value.lifecycle_policy
}

resource "aws_ecr_repository" "replica" {
  for_each = var.replica_enabled ? coalesce(var.repositories, {}) : {}

  name                 = each.value.name
  image_tag_mutability = each.value.image_tag_mutability
  force_delete         = var.force_delete
  tags = merge(
    local.tags,
    var.repositories_tags,
    each.value.tags,
  )

  encryption_configuration {
    encryption_type = local.kms_enabled ? "KMS" : "AES256"
    kms_key         = local.kms_enabled ? local.kms_replica_key_id : null
  }

  provider = aws.replica

  depends_on = [
    aws_kms_replica_key.replica
  ]
}

resource "aws_ecr_repository_policy" "replica" {
  for_each = var.replica_enabled ? local.ecr_repositories_with_policies : {}

  repository = aws_ecr_repository.replica[each.key].name
  policy     = data.aws_iam_policy_document.ecr_internal_repositories[each.key].json

  provider = aws.replica
}

#####
# Data
#####

data "aws_iam_policy_document" "ecr_internal_repositories_base" {
  for_each = local.ecr_repositories_with_policies

  source_policy_documents = each.value.policy != null ? [each.value.policy] : null

  statement {
    sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRPrincipalRestriction"
    effect  = "Deny"
    actions = ["ecr:*"]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "StringNotLike"
      values   = compact(concat(local.iam_policy_scope_restrict_arns, values(coalesce(var.iam_admin_arns, {}))))
      variable = "aws:principalArn"
    }
  }

  dynamic "statement" {
    for_each = length(coalesce(var.iam_policy_restrict_by_account_ids, [])) != 0 ? [1] : []

    content {
      sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRAccountsRestriction"
      effect  = "Deny"
      actions = ["ecr:*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotEquals"
        values   = compact(coalesce(var.iam_policy_restrict_by_account_ids, []))
        variable = "aws:PrincipalAccount"
      }
    }
  }

  lifecycle {
    precondition {
      condition     = (length(local.ecr_repositories_with_policies) > 0 && length(coalesce(var.iam_admin_arns, {})) > 0) || length(local.ecr_repositories_with_policies) == 0
      error_message = "Some ECR repositories contains policies. You need to use “var.iam_admin_arns” to avoid ECR repositories being inaccessible."
    }
  }
}

data "aws_iam_policy_document" "ecr_internal_repositories" {
  for_each = local.ecr_repositories_with_policies

  source_policy_documents = [data.aws_iam_policy_document.ecr_internal_repositories_base[each.key].json]

  dynamic "statement" {
    for_each = each.value.policy_scope != "NONE" ? [1] : []

    content {
      sid    = "${try(chomp(var.iam_policy_sid_prefix), "")}InternalECR${each.value.policy_scope}"
      effect = "Deny"
      not_actions = compact(concat(
        each.value.policy_scope == "FULL" ? ["ecr:*"] : [null],

        each.value.policy_scope == "RWD" ? ["ecr:BatchDeleteImage"] : [null],

        contains(["RW", "RWD"], each.value.policy_scope) ? [
          "ecr:CompleteLayerUpload",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:ReplicateImage",
          "ecr:UploadLayerPart",
        ] : [null],

        contains(["RO", "RW", "RWD"], each.value.policy_scope) ? [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:DescribeImageReplicationStatus",
          "ecr:DescribeImageScanFindings",
          "ecr:DescribeImages",
          "ecr:DescribeRepositories",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetLifecyclePolicy",
          "ecr:ListImages",
        ] : [null],
      ))

      principals {
        type        = "AWS"
        identifiers = ["*"]
      }

      condition {
        test     = "StringLike"
        values   = length(local.iam_policy_scope_restrict_arns) != 0 ? local.iam_policy_scope_restrict_arns : ["*"]
        variable = "aws:principalArn"
      }

      condition {
        test     = "StringNotLike"
        values   = compact(concat(values(coalesce(var.iam_admin_arns, {}))))
        variable = "aws:principalArn"
      }
    }
  }
}

data "aws_iam_policy_document" "ecr_external_repositories" {
  for_each = local.ecr_repositories_with_policies

  statement {
    sid    = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRRead${each.value.name}"
    effect = "Allow"
    actions = compact(concat(
      contains(["RO", "RW", "RWD"], each.value.policy_scope) ? [
        "ecr:BatchCheckLayerAvailability",
        "ecr:BatchGetImage",
        "ecr:DescribeImageReplicationStatus",
        "ecr:DescribeImages",
        "ecr:DescribeRepositories",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetLifecyclePolicy",
        "ecr:ListImages",
      ] : [null],
    ))

    resources = compact([
      aws_ecr_repository.this[each.key].arn,
      try(aws_ecr_repository.replica[each.key].arn, null)
    ])
  }

  statement {
    sid    = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRNonRead${each.value.name}"
    effect = "Allow"
    actions = compact(concat(
      each.value.policy_scope == "FULL" ? ["ecr:*"] : [null],

      each.value.policy_scope == "RWD" ? ["ecr:BatchDeleteImage"] : [null],

      contains(["RW", "RWD"], each.value.policy_scope) ? [
        "ecr:CompleteLayerUpload",
        "ecr:InitiateLayerUpload",
        "ecr:PutImage",
        "ecr:ReplicateImage",
        "ecr:UploadLayerPart",
      ] : [null],
    ))

    resources = [aws_ecr_repository.this[each.key].arn]
  }
}

#####
# Outputs
#####

output "ecr_repositories" {
  value = {
    for key, repository in coalesce(var.repositories, {}) :
    key => {
      arn            = aws_ecr_repository.this[key].arn
      registry_id    = aws_ecr_repository.this[key].registry_id
      repository_url = aws_ecr_repository.this[key].repository_url

      replica_arn            = var.replica_enabled ? aws_ecr_repository.replica[key].arn : null
      replica_registry_id    = var.replica_enabled ? aws_ecr_repository.replica[key].registry_id : null
      replica_repository_url = var.replica_enabled ? aws_ecr_repository.replica[key].repository_url : null

      policy_json = var.repositories_iam_policy_export_json ? try(data.aws_iam_policy_document.ecr_external_repositories[key].json, null) : null
    }
  }
}
