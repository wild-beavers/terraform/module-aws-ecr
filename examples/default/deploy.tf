data "aws_caller_identity" "current" {}

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_group" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test2" {
  name = "${local.prefix}-test2"
}

resource "aws_iam_role" "test" {
  name = "${local.prefix}-test"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = { AWS = local.caller_arn }
      },
    ]
  })
}

#####
# Default
#####

data "aws_iam_policy_document" "default" {
  statement {
    sid = "Default"

    actions = [
      "ecr:ReplicateImage",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

module "default" {
  source = "../../"

  iam_admin_arns                     = { 0 = local.caller_arn }
  iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]

  repositories = {
    minimal = {
      name = "${local.prefix}-default-minimal"
    }

    immutable = {
      name                 = "${local.prefix}-default-immutable"
      image_tag_mutability = "IMMUTABLE"
      policy_scope         = "RO"
      tags = {
        test = "default"
      }
    }

    admin = {
      name         = "${local.prefix}-default-admin"
      policy_scope = "FULL"
      tags = {
        test = "default-admin"
      }
    }

    policy_with_scope = {
      name         = "${local.prefix}-default-policy_with_scope"
      policy_scope = "RW"
      policy       = data.aws_iam_policy_document.default.json
      tags = {
        test = "default-policy"
      }
    }

    policy_without_scope = {
      name         = "${local.prefix}-default-policy_without_scope"
      policy_scope = "NONE"
      policy       = data.aws_iam_policy_document.default.json
      tags = {
        test = "default-policy"
      }
    }

    lifecycle = {
      name             = "${local.prefix}-default-lifecycle"
      policy_scope     = "RW"
      lifecycle_policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
      tags = {
        test = "lifecycle-policy"
      }
    }
  }
  repositories_tags = {
    test          = "default"
    test-extended = "default"
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# IAM
#####

module "iam" {
  source = "../../"

  iam_admin_arns        = { 0 = local.caller_arn }
  iam_policy_group_arns = { 0 = aws_iam_group.test.arn }
  iam_policy_user_arns  = { 0 = aws_iam_user.test.arn, 1 = aws_iam_user.test2.arn }
  iam_policy_role_arns  = { 0 = aws_iam_role.test.arn }

  iam_policy_sid_prefix           = local.prefix
  iam_policy_attach_to_principals = true
  iam_policy_name                 = "${local.prefix}-iam"
  iam_policy_description          = "This is a test policy for ECR"
  iam_policy_tags = {
    test = "ecr-iam"
  }

  repositories = {
    ro = {
      name         = "${local.prefix}-iam-ro"
      policy_scope = "RO"
    }

    rwd = {
      name         = "${local.prefix}-iam-rwd"
      policy_scope = "RWD"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# KMS
#####

module "kms" {
  source = "../../"

  force_delete    = true
  replica_enabled = true

  iam_admin_arns                  = { 0 = local.caller_arn }
  iam_policy_role_arns            = { 0 = aws_iam_role.test.arn }
  iam_policy_attach_to_principals = false
  iam_policy_sid_prefix           = local.prefix

  repositories_iam_policy_export_json = true

  repositories = {
    ro = {
      name         = "${local.prefix}-kms-ro"
      policy_scope = "RO"
    }

    rwd = {
      name         = "${local.prefix}-kms-rwd"
      policy_scope = "RWD"
    }
  }

  kms_key = {
    alias       = "${local.prefix}-kms"
    description = "${local.prefix}-kms KMS key example for ECR"
  }
  kms_tags = {
    test = "ecr-kms"
  }

  providers = {
    aws.replica = aws.replica
  }
}

resource "aws_iam_policy" "kms" {
  name   = "${local.prefix}-kms"
  policy = module.kms.iam_policy_jsons.complete
}

resource "aws_iam_role_policy_attachment" "kms" {
  policy_arn = aws_iam_policy.kms.arn
  role       = aws_iam_role.test.name
}

#####
# External
#####

module "external" {
  source = "../../"

  replica_enabled = true

  iam_policy_sid_prefix = local.prefix
  iam_policy_external_repositories_arns = {
    ro = module.kms.ecr_repositories.ro.arn
  }

  repositories_iam_policy_export_json = true

  kms_key_external_enabled = true
  kms_key_id               = module.kms.kms_key.alias_arn
  kms_replica_key_id       = module.kms.kms_key.multi_region_replica_alias_arn

  providers = {
    aws.replica = aws.replica
  }
}
