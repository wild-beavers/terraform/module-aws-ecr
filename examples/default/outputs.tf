output "default" {
  value = module.default
}

output "iam" {
  value = module.iam
}

output "kms" {
  value = module.kms
}

output "external" {
  value = module.external
}
