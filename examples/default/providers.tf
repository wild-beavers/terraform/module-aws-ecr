provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
  default_tags {
    tags = {
      deployed-by = var.deployer_name
    }
  }

  assume_role {
    session_name = "${var.deployer_name}-terraform"
    role_arn     = var.aws_assume_role
  }
}

provider "aws" {
  region     = "us-west-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
  default_tags {
    tags = {
      deployed-by = var.deployer_name
    }
  }

  assume_role {
    session_name = "${var.deployer_name}-terraform"
    role_arn     = var.aws_assume_role
  }

  alias = "replica"
}
