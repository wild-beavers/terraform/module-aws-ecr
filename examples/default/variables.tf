variable "aws_access_key" {
  type    = string
  default = null
}

variable "aws_secret_key" {
  type    = string
  default = null
}

variable "aws_assume_role" {
  type    = string
  default = null
}

variable "aws_profile" {
  type    = string
  default = null
}

variable "deployer_name" {
  type    = string
  default = ""
}
