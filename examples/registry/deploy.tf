locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

#####
# Registry
#####

data "aws_iam_policy_document" "registry" {
  statement {
    sid       = "RegistryTest"
    effect    = "Deny"
    actions   = ["ecr:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    # This dummy condition allows everyone
    # This is because ECR registries are unique in the whole account
    # A test should not block the ECR registry in any way
    condition {
      test     = "StringNotLike"
      values   = ["*"]
      variable = "aws:principalArn"
    }
  }
}

module "registry" {
  source = "../../"

  ecr_registry_policy_json = data.aws_iam_policy_document.registry.json
  ecr_registry_pull_through_cache_rule = {
    ecr_repository_prefix = "public-${local.prefix}-test"
  }
  ecr_registry_scanning_configuration_rules = {
    SCAN_ON_PUSH = {
      repository_filters = ["example", "another"]
    }

    CONTINUOUS_SCAN = {
      repository_filters = ["this-is-a-wildcard*"]
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}
