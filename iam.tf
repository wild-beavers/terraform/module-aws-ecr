####
# Variables
####

variable "iam_policy_attach_to_principals" {
  description = "Whether to attach auto-generated policies to given `var.iam_policy_group_arns`, `var.iam_policy_user_arns` and `var.iam_policy_role_arns`. For more complex use cases, this be toggled of in favor of `…iam_policy_export_json` variables."
  type        = bool
  default     = false
}

variable "iam_policy_external_repositories_enabled" {
  description = "When `true` user of this module is expected to set `var.iam_policy_external_repositories_arn` with non null values."
  type        = bool
  default     = false
}

variable "iam_policy_external_repositories_arns" {
  description = "ARNs of ECR repositories created outside this module to compose the external policies with. Along with repositories created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_policy_external_repositories_arns, {}) :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:ecr:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):repository/[a-z[0-9a-z\\/\\._-]{2,256}$", arn)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_external_repositories_arns” are invalid."
  }
}

variable "iam_policy_name" {
  type        = string
  description = "Name of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`."
  default     = null

  validation {
    condition     = var.iam_policy_name == null || can(regex("^[a-zA-Z0-9+=,\\./@-]+$", var.iam_policy_name))
    error_message = "“var.iam_policy_name” is invalid. Check the requirements in the variables.tf file."
  }
}

variable "iam_policy_path" {
  type        = string
  description = "Path of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`."
  default     = null

  validation {
    condition     = var.iam_policy_path == null || can(regex("^\\/.*$", var.iam_policy_path))
    error_message = "“var.iam_policy_path” is invalid. Check the requirements in the variables.tf file."
  }
}

variable "iam_policy_description" {
  type        = string
  description = "Description of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`."
  default     = null

  validation {
    condition     = var.iam_policy_description == null || (1 <= length(coalesce(var.iam_policy_description, "empty")) && length(coalesce(var.iam_policy_description, "empty")) <= 1024)
    error_message = "“var.iam_policy_description” is invalid. Check the requirements in the variables.tf file."
  }
}

variable "iam_policy_tags" {
  type        = map(string)
  default     = null
  description = "Tags of the policy to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`."
}

####
# Locals
####

locals {
  iam_should_generate_policy      = var.iam_policy_group_arns != null || var.iam_policy_role_arns != null || var.iam_policy_user_arns != null || var.iam_policy_external_repositories_arns != null || var.repositories_iam_policy_export_json || var.iam_policy_external_repositories_enabled
  iam_should_generate_ro_policy   = local.iam_should_generate_policy
  iam_should_generate_rw_policy   = local.iam_should_generate_policy && (length(local.ecr_repositories_with_no_policy_or_at_least_rw) != 0 || var.iam_policy_external_repositories_enabled)
  iam_should_generate_rwd_policy  = local.iam_should_generate_policy && (length(local.ecr_repositories_with_no_policy_or_at_least_rwd) != 0 || var.iam_policy_external_repositories_enabled)
  iam_should_generate_full_policy = local.iam_should_generate_policy && (length(local.ecr_repositories_with_no_policy_or_full) != 0 || var.iam_policy_external_repositories_enabled)

  iam_should_generate_kms_rw_policy  = local.iam_should_generate_policy && local.kms_enabled
  iam_should_generate_kms_rwd_policy = local.iam_should_generate_rwd_policy && local.kms_enabled

  ecr_repositories_with_no_policy_or_at_least_rw = {
    for key, repository in coalesce(var.repositories, {}) :
    key => repository if repository.policy != null || contains(["NONE", "RW", "RWD", "FULL"], repository.policy_scope)
  }
  ecr_repositories_with_no_policy_or_at_least_rwd = {
    for key, repository in coalesce(var.repositories, {}) :
    key => repository if repository.policy != null || contains(["NONE", "RWD", "FULL"], repository.policy_scope)
  }
  ecr_repositories_with_no_policy_or_full = {
    for key, repository in coalesce(var.repositories, {}) :
    key => repository if repository.policy != null || contains(["NONE", "FULL"], repository.policy_scope)
  }
}

####
# Resources
####

resource "aws_iam_policy" "this" {
  for_each = var.iam_policy_attach_to_principals ? { 0 = 0 } : {}

  name   = var.iam_policy_name
  path   = coalesce(var.iam_policy_path, "/")
  policy = data.aws_iam_policy_document.ecr_all["0"].json

  description = var.iam_policy_description

  tags = merge(
    {
      Name        = var.iam_policy_name
      Description = var.iam_policy_description
    },
    local.tags,
    var.iam_policy_tags,
  )
}

resource "aws_iam_group_policy_attachment" "this" {
  for_each = var.iam_policy_attach_to_principals ? coalesce(var.iam_policy_group_arns, {}) : {}

  policy_arn = aws_iam_policy.this["0"].arn
  group      = split(":group/", each.value)[1]
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = var.iam_policy_attach_to_principals ? coalesce(var.iam_policy_role_arns, {}) : {}

  policy_arn = aws_iam_policy.this["0"].arn
  role       = split(":role/", each.value)[1]
}

resource "aws_iam_user_policy_attachment" "this" {
  for_each = var.iam_policy_attach_to_principals ? coalesce(var.iam_policy_user_arns, {}) : {}

  policy_arn = aws_iam_policy.this["0"].arn
  user       = split(":user/", each.value)[1]
}

####
# Data
####

data "aws_iam_policy_document" "external_ecr_kms_rw" {
  for_each = local.iam_should_generate_kms_rw_policy ? { 0 = 0 } : {}

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRKMSReadWrite"

    effect = "Allow"

    actions = [
      "kms:CreateGrant",
      "kms:Decrypt",
      "kms:DescribeKey",
    ]

    resources = compact([
      local.kms_key_id,
      var.replica_enabled ? local.kms_replica_key_id : null,
    ])
  }
}

data "aws_iam_policy_document" "external_ecr_kms_rwd" {
  for_each = local.iam_should_generate_kms_rwd_policy ? { 0 = 0 } : {}

  source_policy_documents = [data.aws_iam_policy_document.external_ecr_kms_rw["0"].json]

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRKMSDelete"

    effect = "Allow"

    actions = [
      "kms:RetireGrant",
    ]

    resources = compact([
      local.kms_key_id,
      var.replica_enabled ? local.kms_replica_key_id : null,
    ])
  }
}

data "aws_iam_policy_document" "ecr_read_statement" {
  for_each = local.iam_should_generate_ro_policy ? { 0 = 0 } : {}

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRLogin"

    actions = [
      "ecr:GetAuthorizationToken",
    ]

    resources = [
      "*"
    ]
  }

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRRead"

    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:DescribeImageReplicationStatus",
      "ecr:DescribeImageScanFindings",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetLifecyclePolicy",
      "ecr:GetDownloadUrlForLayer",
      "ecr:ListImages",
    ]

    resources = concat(flatten([for key, repository in coalesce(var.repositories, {}) :
      compact([
        aws_ecr_repository.this[key].arn,
        try(aws_ecr_repository.replica[key].arn, null)
      ])
    ]), values(coalesce(var.iam_policy_external_repositories_arns, {})))
  }
}

data "aws_iam_policy_document" "ecr_read_write_statement" {
  for_each = local.iam_should_generate_rw_policy ? { 0 = 0 } : {}

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRWrite"

    actions = [
      "ecr:CompleteLayerUpload",
      "ecr:InitiateLayerUpload",
      "ecr:PutImage",
      "ecr:ReplicateImage",
      "ecr:UploadLayerPart",
    ]

    resources = concat(flatten([for key, repository in local.ecr_repositories_with_no_policy_or_at_least_rw :
      compact([
        aws_ecr_repository.this[key].arn,
      ])
    ]), values(coalesce(var.iam_policy_external_repositories_arns, {})))
  }
}

data "aws_iam_policy_document" "ecr_read_write_delete_statement" {
  for_each = local.iam_should_generate_rwd_policy ? { 0 = 0 } : {}

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRDelete"

    actions = [
      "ecr:BatchDeleteImage",
    ]

    resources = concat(flatten([for key, repository in local.ecr_repositories_with_no_policy_or_at_least_rwd :
      compact([
        aws_ecr_repository.this[key].arn,
      ])
    ]), values(coalesce(var.iam_policy_external_repositories_arns, {})))
  }
}

data "aws_iam_policy_document" "ecr_full_statement" {
  for_each = local.iam_should_generate_full_policy ? { 0 = 0 } : {}

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}ECRFull"

    actions = [
      "ecr:*",
    ]

    resources = concat(flatten([for key, repository in local.ecr_repositories_with_no_policy_or_full :
      compact([
        aws_ecr_repository.this[key].arn,
      ])
    ]), values(coalesce(var.iam_policy_external_repositories_arns, {})))
  }
}

data "aws_iam_policy_document" "ecr_full" {
  for_each = local.iam_should_generate_full_policy ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [data.aws_iam_policy_document.ecr_full_statement["0"].json],
    local.kms_enabled ? [data.aws_iam_policy_document.external_ecr_kms_rwd["0"].json] : [null]
  ))
}

data "aws_iam_policy_document" "ecr_read_write_delete" {
  for_each = local.iam_should_generate_rwd_policy ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [
      data.aws_iam_policy_document.ecr_read_write_delete_statement["0"].json,
      data.aws_iam_policy_document.ecr_read_write_statement["0"].json,
      data.aws_iam_policy_document.ecr_read_statement["0"].json,
    ],
    local.kms_enabled ? [data.aws_iam_policy_document.external_ecr_kms_rwd["0"].json] : [null]
  ))
}

data "aws_iam_policy_document" "ecr_read_write" {
  for_each = local.iam_should_generate_rw_policy ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [
      data.aws_iam_policy_document.ecr_read_write_statement["0"].json,
      data.aws_iam_policy_document.ecr_read_statement["0"].json,
    ],
    local.kms_enabled ? [data.aws_iam_policy_document.external_ecr_kms_rw["0"].json] : [null]
  ))
}

data "aws_iam_policy_document" "ecr_read" {
  for_each = local.iam_should_generate_ro_policy ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [
      data.aws_iam_policy_document.ecr_read_statement["0"].json,
    ],
    local.kms_enabled ? [data.aws_iam_policy_document.external_ecr_kms_rw["0"].json] : [null]
  ))
}

data "aws_iam_policy_document" "ecr_all" {
  for_each = local.iam_should_generate_policy ? { 0 = 0 } : {}

  source_policy_documents = compact([
    try(data.aws_iam_policy_document.ecr_full_statement["0"].json, null),
    try(data.aws_iam_policy_document.ecr_read_write_delete_statement["0"].json, null),
    try(data.aws_iam_policy_document.ecr_read_write_statement["0"].json, null),
    try(data.aws_iam_policy_document.ecr_read_statement["0"].json, null),
    try(data.aws_iam_policy_document.external_ecr_kms_rwd["0"].json, data.aws_iam_policy_document.external_ecr_kms_rw["0"].json, null)
    ]
  )
}

####
# Outputs
####

output "iam_policy_jsons" {
  value = var.repositories_iam_policy_export_json ? {
    full = try(data.aws_iam_policy_document.ecr_full["0"].json, null)
    ro   = try(data.aws_iam_policy_document.ecr_read["0"].json, null)
    rw   = try(data.aws_iam_policy_document.ecr_read_write["0"].json, null)
    rwd  = try(data.aws_iam_policy_document.ecr_read_write_delete["0"].json, null)

    complete = try(data.aws_iam_policy_document.ecr_all["0"].json, null)

    kms_ro  = try(data.aws_iam_policy_document.external_ecr_kms_rw["0"].json, null)
    kms_rw  = try(data.aws_iam_policy_document.external_ecr_kms_rw["0"].json, null)
    kms_rwd = try(data.aws_iam_policy_document.external_ecr_kms_rwd["0"].json, null)
  } : null
}

output "iam_policy" {
  value = var.iam_policy_attach_to_principals ? {
    arn  = aws_iam_policy.this["0"].arn
    id   = aws_iam_policy.this["0"].id
    name = aws_iam_policy.this["0"].name
  } : null
}
