#####
# Variables
#####

variable "kms_key_external_enabled" {
  description = "If `true`, you declare `kms_key_id` and `kms_replica_key_id` are not empty. https://github.com/hashicorp/terraform/issues/33620."
  type        = bool
  default     = false
}

variable "kms_key_id" {
  description = "ARN, Key ID, or Alias of the AWS KMS to be used to encrypt the resources data of this module. If you don't specify this value or `var.kms_key`, then ECR defaults to using the AWS account's default CMK (the one named `aws/ecr`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time."
  type        = string
  default     = null
}

variable "kms_replica_key_id" {
  description = "ARN, Key ID, or Alias of the AWS KMS key within the region ECR is replicated to. If one is not specified, then ECR defaults to using the AWS account's default KMS key. If `null` and `kms_key_alias` is set, a replica of the KMS key will be used. Ignored anyway if `var.replica_enabled` is `false`."
  type        = string
  default     = null
}

variable "kms_tags" {
  description = "Tags to be shared with all KMS resources. Will be merge with `var.tags`."
  type        = map(string)
  default     = null
}

variable "kms_key" {
  description = <<-DOCUMENTATION
Manage the KMS key for the repositories.
If given and and `var.replica_enabled`, this key will be use also for the replica, unless `var.kms_replica_key_id` is set.
Keys are free values.

  * alias                   (required, string):             KMS key alias; Display name of the KMS key. Omit the `alias/`.
  * policy_json             (optional, string):             Internal policy in JSON to attach to the KMS key. This module will grant the bare minimum for ECR and merge it with this policy.
  * default_policy          (optional, bool, false):        Whether to use restricted policy for the key or the AWS default open policy.
  * description             (optional, string, "For ECR."): Description of the key.
  * rotation_enabled        (optional, bool, true):         Whether to automatically rotate the KMS key linked to the ECR.
  * deletion_window_in_days (optional, number, 7):          The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  * tags                    (optional, map(string)):        Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = string
    policy_json             = optional(string)
    default_policy          = optional(bool, false)
    description             = optional(string, "For ECR.")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null

  validation {
    condition = var.kms_key == null || (
      (try(var.kms_key.alias, null) == null || can(regex("[A-Za-z\\-_\\/]{1,256}", try(var.kms_key.alias, null)))) &&
      (try(var.kms_key.policy_json, null) == null || try(jsondecode(var.kms_key.policy_json), null) != null) &&
      (try(var.kms_key.description, null) == null || (1 <= length(coalesce(try(var.kms_key.description, null), "empty")) && length(coalesce(try(var.kms_key.description, null), "empty")) <= 512)) &&
      (try(var.kms_key.deletion_window_in_days, null) == null || 7 <= coalesce(try(var.kms_key.deletion_window_in_days, null), 7) && coalesce(try(var.kms_key.deletion_window_in_days, null), 7) <= 30)
    )
    error_message = "“try(var.kms_key” is invalid. Check the requirements in the variables.tf file."
  }
}

#####
# Locals
#####

locals {
  kms_enabled = local.kms_should_create || local.kms_should_use_external

  kms_should_create       = var.kms_key != null && !local.kms_should_use_external
  kms_should_use_external = var.kms_key_external_enabled
  kms_key_id              = local.kms_enabled ? (local.kms_should_use_external ? var.kms_key_id : aws_kms_key.this["0"].arn) : null

  kms_replica_should_create       = var.replica_enabled && local.kms_should_create
  kms_replica_should_use_external = var.kms_key_external_enabled
  kms_replica_key_id              = local.kms_replica_should_create ? (local.kms_replica_should_use_external ? var.kms_replica_key_id : aws_kms_replica_key.replica["0"].arn) : null
}

#####
# Resources
#####

resource "aws_kms_key" "this" {
  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  description             = each.value.description
  enable_key_rotation     = each.value.rotation_enabled
  deletion_window_in_days = each.value.deletion_window_in_days
  multi_region            = var.replica_enabled

  tags = merge(
    {
      Name        = each.value.alias
      Description = each.value.description
    },
    local.tags,
    var.kms_tags,
    each.value.tags,
  )
}

resource "aws_kms_alias" "this" {
  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  name          = "alias/${each.value.alias}"
  target_key_id = aws_kms_key.this[each.key].key_id
}

resource "aws_kms_key_policy" "this" {
  for_each = local.kms_should_create && !try(var.kms_key.default_policy, false) ? { 0 = var.kms_key } : {}

  key_id = aws_kms_key.this[each.key].key_id
  policy = data.aws_iam_policy_document.internal_ecr_kms["0"].json
}

resource "aws_kms_replica_key" "replica" {
  for_each = local.kms_replica_should_create ? { 0 = var.kms_key } : {}

  primary_key_arn = aws_kms_key.this[each.key].arn

  description             = coalesce(each.value.description, "Replica key for ECR.")
  deletion_window_in_days = each.value.deletion_window_in_days

  tags = merge(
    {
      Name        = each.value.alias
      Description = coalesce(each.value.description, "Replica key for ECR.")
    },
    local.tags,
    var.kms_tags,
    each.value.tags,
  )

  provider = aws.replica
}

resource "aws_kms_alias" "replica" {
  for_each = local.kms_replica_should_create ? { 0 = var.kms_key } : {}

  name          = "alias/${each.value.alias}"
  target_key_id = aws_kms_replica_key.replica[each.key].key_id

  provider = aws.replica
}

resource "aws_kms_key_policy" "replica" {
  for_each = local.kms_replica_should_create && !try(var.kms_key.default_policy, false) ? { 0 = var.kms_key } : {}

  key_id = aws_kms_replica_key.replica[each.key].id
  policy = data.aws_iam_policy_document.internal_ecr_kms["0"].json

  provider = aws.replica
}

#####
# Data
#####

data "aws_iam_policy_document" "internal_ecr_kms" {
  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  source_policy_documents = each.value.policy_json != null ? [each.value.policy_json] : null

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}InternalKMSECR"

    actions = compact([
      "kms:CreateGrant",
      "kms:DescribeKey",
      "kms:RetireGrant",
      "kms:GenerateDataKey",
      "kms:Decrypt",
    ])

    resources = ["*"]

    principals {
      identifiers = ["ecr.amazonaws.com"]
      type        = "Service"
    }
  }

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}InternalKMSECRPrincipals"

    actions = [
      "kms:CreateGrant",
      "kms:DescribeKey",
      "kms:RetireGrant",
    ]

    resources = ["*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringLike"
      values   = length(local.iam_policy_scope_restrict_arns) != 0 ? local.iam_policy_scope_restrict_arns : ["*"]
      variable = "aws:principalArn"
    }
  }

  dynamic "statement" {
    for_each = length(coalesce(var.iam_policy_restrict_by_account_ids, [])) != 0 ? [1] : []

    content {
      sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}InternalKMSAccountsRestriction"
      effect  = "Deny"
      actions = ["ecr:*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotEquals"
        values   = compact(coalesce(var.iam_policy_restrict_by_account_ids, []))
        variable = "aws:PrincipalAccount"
      }
    }
  }

  statement {
    sid = "${try(chomp(var.iam_policy_sid_prefix), "")}InternalKMSAdmins"

    actions = [
      "kms:*",
    ]

    resources = ["*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringLike"
      values   = compact(concat(values(coalesce(var.iam_admin_arns, {}))))
      variable = "aws:principalArn"
    }
  }
}

#####
# Outputs
#####

output "kms_key" {
  value = local.kms_should_create ? {
    alias                          = aws_kms_alias.this["0"].name
    alias_arn                      = aws_kms_alias.this["0"].arn
    arn                            = aws_kms_key.this["0"].arn
    id                             = aws_kms_key.this["0"].id
    key_id                         = aws_kms_key.this["0"].key_id
    multi_region                   = aws_kms_key.this["0"].multi_region
    multi_region_replica_alias     = local.kms_replica_should_create ? aws_kms_alias.replica["0"].name : null
    multi_region_replica_alias_arn = local.kms_replica_should_create ? aws_kms_alias.replica["0"].arn : null
    multi_region_replica_arn       = local.kms_replica_should_create ? aws_kms_replica_key.replica["0"].arn : null
    multi_region_replica_id        = local.kms_replica_should_create ? aws_kms_replica_key.replica["0"].id : null
    multi_region_replica_key_id    = local.kms_replica_should_create ? aws_kms_replica_key.replica["0"].key_id : null
    multi_region_replica_key_spec  = local.kms_replica_should_create ? aws_kms_replica_key.replica["0"].key_spec : null
  } : null
}
