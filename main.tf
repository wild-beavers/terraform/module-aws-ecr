#####
# Variables
#####

variable "tags" {
  description = "Tags to be shared with all resources of this module."
  type        = map(string)
  default     = null
}

variable "force_delete" {
  description = "If `true`, will delete the resources of this module even if they contain data. E.g. if `true`, ECR will be deleted even if they contain images. Defaults to `false`."
  type        = bool
  default     = false
}

variable "iam_admin_arns" {
  description = "ARNs of the Administrators for all the resources of this module. If you set any policy, this is mandatory. Secrets will grant unrestricted access to these ARNs. Wildcards are authorized. `var.iam_policy_attach_to_principals` will have no effect on these resources. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_admin_arns, {}) :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):(federated-user|user|role)/[a-zA-Z0-9+=\\*,\\./@-]+$", arn)))
      )
    ], false)))
    error_message = "One or more “var.iam_admin_arns” are invalid."
  }
}

variable "iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies - internal and external - created by this module."
  type        = string
  default     = null
}

variable "iam_policy_group_arns" {
  description = "Restrict the access to resources of this module containing policies to the given groups. Used in addition of `var.iam_policy_user_arns` and `var.iam_policy_role_arns`. Wildcards are **not** accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_policy_group_arns, {}) :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:group/[a-zA-Z0-9+=,\\./@-]+$", arn)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_group_arns” are invalid."
  }
}

variable "iam_policy_user_arns" {
  description = "Restrict the access to resources of this module containing policies to the given users. Used in addition of `var.iam_policy_group_arns` and `var.iam_policy_role_arns`. Wildcards and variables are accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs, but will not work with wildcards. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_policy_user_arns, {}) :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws|\\*):user/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_user_arns” are invalid."
  }
}

variable "iam_policy_role_arns" {
  description = "Restrict the access to resources of this module containing policies to the given roles. Used in addition of `var.iam_policy_group_arns` and `var.iam_policy_user_arns`. Wildcards and variables are accepted. If `var.iam_policy_attach_to_principals` is `true`, a policy will be attached to the given ARNs, but will not work with wildcards. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_policy_role_arns, {}) :
      (
        (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws|\\*):role/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_role_arns” are invalid."
  }
}

variable "replica_enabled" {
  description = "If `true`, ECR will be replicated into the `aws.replica` region."
  type        = bool
  default     = false
}

variable "iam_policy_restrict_by_account_ids" {
  description = "Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = null

  validation {
    condition = ((!contains([
      for id in coalesce(var.iam_policy_restrict_by_account_ids, []) :
      (
        (can(regex("^[0-9]{12}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_account_ids” are invalid."
  }
}

#####
# Locals
#####

locals {
  iam_policy_scope_restrict_arns = compact(concat(
    values(coalesce(var.iam_policy_role_arns, {})),
    values(coalesce(var.iam_policy_user_arns, {})),
    flatten([for group in data.aws_iam_group.policy_group : [for user in group.users : user.arn]]),
  ))
  tags = merge(
    {
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-ecr"
      managed-by = "terraform"
    },
    var.tags
  )
}

#####
# Data
#####

data "aws_region" "replica" {
  for_each = var.replica_enabled ? { 0 = 0 } : {}

  provider = aws.replica
}

data "aws_caller_identity" "current" {}

data "aws_iam_group" "policy_group" {
  for_each = coalesce(var.iam_policy_group_arns, {})

  group_name = split(":group/", each.value)[1]
}
